import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-survey-list-item',
  templateUrl: './survey-list-item.component.html',
  styleUrls: ['./survey-list-item.component.css']
})
export class SurveyListItemComponent {

  @Input() survey;
  @Output() clickSurvey: EventEmitter<number> = new EventEmitter();

  constructor() { }

  onSurveyItemClicked() {
    this.clickSurvey.emit(this.survey.survey_id);
  }

}
