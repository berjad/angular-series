import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      user: { ...user }
    }).toPromise();
  }

  login(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, {
      user: { ...user }
    }).pipe(

      map((response: any) => {

        if (response.status >= 400 || response.error != "") {
          // We have an error :(
          return throwError(response.error);
        }

        return {
          username: response.data.user.username,
          token: response.data.token
        }

      }),

      catchError(errorResponse => {
        const { error } = errorResponse.error || errorResponse.message;
        return throwError(error);
      })

    ).toPromise();
  }

}
